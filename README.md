# PythonRequest

#### 介绍

基于微信开放平台开发的接口自动化框架
实时更新

#### 软件架构
使用python+request+unittest+allure

#### 代码执行顺序

![执行顺序](%E6%9C%AA%E5%91%BD%E5%90%8D%E6%96%87%E4%BB%B6.jpg)

#### 博客地址

https://blog.csdn.net/weixin_42382016/category_11587744.html?spm=1001.2014.3001.5482