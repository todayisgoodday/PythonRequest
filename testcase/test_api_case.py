# -*- coding: utf-8 -*-
# @Time : 2021/12/22 13:47
# @Author : Limusen
# @File : test_api_case

import unittest
import paramunittest
from common.testcase_data_utils import TestCaseDataUtils
from common.request_utils import RequestsUtils

testcase_data_lists = TestCaseDataUtils().convert_data_info_list()


@paramunittest.parametrized(
    *testcase_data_lists
)
class TestApiCase(paramunittest.ParametrizedTestCase):

    def setParameters(self, case_id, case_step):
        self.case_id = case_id
        self.case_step = case_step

    def test_api_case(self):
        test_result = RequestsUtils().request_steps(self.case_step)
        self.assertTrue(test_result["check_result"])


if __name__ == '__main__':
    unittest.main(verbosity=2)
