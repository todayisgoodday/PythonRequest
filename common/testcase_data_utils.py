# -*- coding: utf-8 -*-
# @Time : 2021/12/9 13:42
# @Author : Limusen
# @File : testcase_data_utils

import os
from common.excel_utils import ExcelUtils

current = os.path.dirname(os.path.abspath(__file__))
excel_file_path = os.path.join(current, '..', 'testcase_data', 'testcase_infos.xlsx')
excel_sheet_name = "Sheet1"


class TestCaseDataUtils:

    def __init__(self):
        self.ex = ExcelUtils(excel_file_path=excel_file_path,
                             sheet_name=excel_sheet_name)

    def convert_data_info_dict(self):
        """
        将测试数据转换成字典
        :param testcase_info:
        :return:
        """
        test_case_dict = {}
        for testcase in self.ex.get_all_excel_data():
            if testcase["用例执行"] == "是":
                test_case_dict.setdefault(testcase['测试用例编号'], []).append(testcase)
        return test_case_dict

    def convert_data_info_list(self):
        test_case_list = []
        for key, value in self.convert_data_info_dict().items():
            type_dict = {}
            type_dict["case_id"] = key
            type_dict["case_step"] = value
            test_case_list.append(type_dict)
        return test_case_list

if __name__ == '__main__':
    print(TestCaseDataUtils().convert_data_info_list())
