# -*- coding: utf-8 -*-
# @Time : 2021/12/8 15:48
# @Author : Limusen
# @File : config_utils

import os
import configparser

# 优化获取文件路径的方式
current_path = os.path.dirname(os.path.abspath(__file__))
# 这里分开写是为了适配mac跟windows之间的// 转义,这样更兼容
conf_file_path = os.path.join(current_path, '..', 'conf', 'conf.ini')

"""
# 优化前
# cf = configparser.ConfigParser()
# cf.read("D:\PythonRequest\conf\conf.ini", encoding="utf-8")
# print(cf.get("default", "hosts"))

# 优化后
# cf = configparser.ConfigParser()
# cf.read(conf_file_path, encoding="utf-8")
# print(cf.get("default", "hosts"))

# 接下来封装成类方法
"""


class ConfigUtils:

    def __init__(self, conf_path=conf_file_path):
        # 做一个默认传入配置文件的路径
        self.cf = configparser.ConfigParser()
        self.cf.read(conf_path, encoding="utf-8")

    @property  # 将类方法变成类属性 方便调用
    def get_hosts(self):
        hosts = self.cf.get("default", "hosts")
        return hosts

    @property
    def LOG_PATH(self):
        log_path = self.cf.get("path", "log_path")  # 获取值
        return log_path

    @property
    def LOG_NAME(self):
        log_name = self.cf.get("default", "log_name")  # 获取值
        return log_name

    @property
    def CASE_PATH(self):
        case_path = self.cf.get("path", "case_path")  # 获取值
        return case_path

    @property
    def REPORT_PATH(self):
        report_path = self.cf.get("path", "report_path")  # 获取值
        return report_path

local_config = ConfigUtils()

if __name__ == '__main__':
    # 两种方法均可使用
    # 方法一
    cf = ConfigUtils()
    print(cf.get_hosts)
    # 方法二
    print(local_config.REPORT_PATH)
