# -*- coding: utf-8 -*-
# @Time : 2021/12/8 16:40
# @Author : Limusen
# @File : excel_utils

import os
import xlrd3

# 优化获取文件路径的方式
current_path = os.path.dirname(os.path.abspath(__file__))
# 这里分开写是为了适配mac跟windows之间的// 转义,这样更兼容
excel_file_path = os.path.join(current_path, '..', 'testcase_data', '做饭秘籍.xlsx')
sheet_name = "Sheet1"


class ExcelUtils:

    def __init__(self, excel_file_path, sheet_name):
        self.excel_file_path = excel_file_path
        self.sheet_name = sheet_name
        # 可以在构造方法中调用实例化的方法
        self.sheet_obj = self.get_sheet_object()

    def get_sheet_object(self):
        """
        创建文件对象
        :return:
        """
        workbook_obj = xlrd3.open_workbook(self.excel_file_path)
        sheet_obj = workbook_obj.sheet_by_name(self.sheet_name)
        return sheet_obj

    def get_row_count(self):
        """
        获取总行数
        :return:
        """
        return self.sheet_obj.nrows

    def get_col_count(self):
        """
        获取总列数
        :return:
        """
        return self.sheet_obj.ncols

    def get_merged_value(self, row_index, col_index):
        """
        查询指定单元格信息
        :param row_index: 想要查询的行坐标
        :param col_index: 想要查询的类坐标
        :return:
        """
        for (min_row, max_row, min_col, max_col) in self.sheet_obj.merged_cells:

            if row_index >= min_row and row_index < max_row:
                if col_index >= min_col and col_index < max_col:
                    # 是合并单元格,单元格的值等于 合并单元格中的第一个单元格的值
                    cell_value = self.sheet_obj.cell_value(min_row, min_col)
                    break
                else:
                    cell_value = self.sheet_obj.cell_value(row_index, col_index)
            else:
                cell_value = self.sheet_obj.cell_value(row_index, col_index)
        return cell_value

    def get_all_excel_data(self):
        """
        获取全部excel表格数据 用列表组装
        :return: excel数据
        """
        excel_data_list = []
        for i in range(1, self.get_row_count()):
            # 先定义一个空字典
            row_dict = {}
            # 再根据行数 循环列数
            for j in range(self.get_col_count()):
                # 重新赋值
                row_dict[self.get_merged_value(0, j)] = self.get_merged_value(i, j)
            excel_data_list.append(row_dict)
        return excel_data_list


if __name__ == '__main__':
    # 每次做完都需要检查一下自己代码是否正确噢!
    ex = ExcelUtils(excel_file_path, "Sheet1")
    print(ex.get_merged_value(7, 0))

    print(ex.get_all_excel_data())
