# -*- coding: utf-8 -*-
# @Time : 2021/12/13 14:07
# @Author : Limusen
# @File : check_utils

import json
import re
from common.logs_utils import logger


class CheckUtils:

    def __init__(self, response_data):
        """
        :param response_data: 响应结果
        """
        self.response_data = response_data
        self.function = {
            "none": self.none_check,
            "json_key": self.key_check,
            "json_key_value": self.key_value_check,
            "body_regexp": self.body_regexp_check,
            "header_key_check": self.header_key_check,
            "header_key_value_check": self.header_key_value_check,
            "response_code_check": self.response_code_check,
            "contain_text": self.__contain_text
        }
        self.pass_result = {  # 通过结果
            "code": 0,
            "response_code": self.response_data.status_code,
            "response_reason": self.response_data.reason,
            "response_headers": self.response_data.headers,
            "response_body": self.response_data.text,
            "message": "测试用例执行通过",
            "check_result": True
        }
        self.fail_result = {  # 失败结果 2 表示断言失败
            "code": 2,
            "response_code": self.response_data.status_code,
            "response_reason": self.response_data.reason,
            "response_headers": self.response_data.headers,
            "response_body": self.response_data.text,
            "message": "测试用例断言失败，测试用例执行不通过",
            "check_result": False
        }

    def none_check(self):
        """
        断言类型为空的情况
        :return:
        """
        logger.info("断言类型 [none],不进行断言，本次断言通过")
        return self.pass_result

    def key_check(self, check_data):
        """
        检查键是否相同
        :param check_data: 需要检查的字段,注意得是字符串才行,因为要分割
        :return: True说明断言成功，False说明断言失败
        """
        # 字符串逗号分割
        key_list = check_data.split(",")
        tmp_result = []
        # 取出需要断言的字段
        for check_key in key_list:
            # 如果 check_key 在json串的键当中，则添加True，不是则添加False
            if check_key in self.response_data.json().keys():
                tmp_result.append(True)
            else:
                tmp_result.append(False)
        if False in tmp_result:
            logger.error('断言类型 [key_check] ==》实际结果：{}, 期望结果：{} 不相符，断言失败'.format(self.response_data.text, check_data))
            # 只要有一个不符合 用例全部失败
            return self.fail_result
        else:
            logger.info("断言类型 [key_check],检查键值，实际结果：{}, 期望结果：{} 相符，断言成功".format(self.response_data.text, check_data))
            return self.pass_result

    def key_value_check(self, check_data):
        """
        检查键值对是否一致
        :param check_data:
        :return:
        """
        key_dict = json.loads(check_data)
        tmp_result = []

        for check_key in key_dict.items():
            if check_key in self.response_data.json().items():
                tmp_result.append(True)
            else:
                tmp_result.append(False)
        if False in tmp_result:
            logger.error(
                '断言类型 [key_value_check] ==》实际结果：{}, 期望结果：{} 不相符，断言失败'.format(self.response_data.text, check_data
                                                                             ))
            return self.fail_result
        else:
            logger.info('断言类型 [key_value_check] ==》实际结果：{}, 期望结果：{} 相符，断言成功'.format(self.response_data.text, check_data))

            return self.pass_result

    def body_regexp_check(self, check_data):
        """
        根据正则表达式断言
        :param check_data:
        :return:
        """
        if re.findall(check_data, self.response_data.text):
            # 能找到check_data的值则算通过
            logger.info(
                '断言类型 [body_regexp_check] ==》实际结果：{}, 期望结果：{} 相符，断言成功'.format(self.response_data.text, check_data
                                                                              ))
            return self.pass_result
        else:
            logger.error(
                '断言类型 [body_regexp_check] ==》实际结果：{}, 期望结果：{} 不相符，断言失败'.format(self.response_data.text, check_data
                                                                               ))
            return self.fail_result

    def header_key_check(self, check_data):
        """
        检查头部信息是否包含某个值  可以参照key_check()
        :param check_data:
        :return:
        """
        # 字符串逗号分割
        key_list = check_data.split(",")
        tmp_result = []
        # 取出需要断言的字段
        for check_key in key_list:
            # 如果 check_key 在json串的键当中，则添加True，不是则添加False
            if check_key in self.response_data.headers.keys():
                tmp_result.append(True)
            else:
                tmp_result.append(False)
        if False in tmp_result:
            logger.error(
                '断言类型 [header_key_check] ==》实际结果：{}, 期望结果：{} 不相符，断言失败'.format(self.response_data.text, check_data
                                                                              ))
            # 只要有一个不符合 用例全部失败
            return self.fail_result
        else:
            logger.info(
                '断言类型 [header_key_check] ==》实际结果：{}, 期望结果：{} 相符，断言成功'.format(self.response_data.text, check_data
                                                                             ))
            return self.pass_result

    def header_key_value_check(self, check_data):
        """
        检查头部键值对是否一致 参照key_value_check()
        :param check_data:
        :return:
        """
        key_dict = json.loads(check_data)
        tmp_result = []

        for check_key in key_dict.items():
            if check_key in self.response_data.headers.items():
                tmp_result.append(True)
            else:
                tmp_result.append(False)
        if False in tmp_result:
            logger.error(
                '断言类型 [header_key_value_check] ==》实际结果：{}, 期望结果：{} 不相符，断言失败'.format(self.response_data.text, check_data
                                                                                    ))
            # 只要有一个不符合 用例全部失败
            return self.fail_result
        else:
            logger.info(
                '断言类型 [header_key_value_check] ==》实际结果：{}, 期望结果：{} 相符，断言成功'.format(self.response_data.text, check_data
                                                                                   ))
            return self.pass_result

    def response_code_check(self, check_data):
        """
        检查返回状态码
        :param check_data:
        :return:
        """
        if self.response_data.status_code == int(check_data):
            logger.info(
                '断言类型 [__response_code_check] ==》实际结果：{}, 期望结果：{} 相符，断言成功'.format(self.response_data.text, check_data
                                                                                  ))
            return self.pass_result
        else:
            logger.error(
                '断言类型 [__response_code_check] ==》实际结果：{}, 期望结果：{} 不相符，断言失败'.format(self.response_data.text, check_data
                                                                                   ))
            return self.fail_result

    def __contain_text(self, check_data):
        """
        断言数据中是否存在该字段
        :param check_data:
        :return:
        """
        if check_data in self.response_data.text:
            logger.info(
                '断言类型 [__contain_text] ==》实际结果：{}, 期望结果：{} 相符，断言成功'.format(self.response_data.text, check_data
                                                                          ))
            return self.pass_result
        else:
            logger.error(
                '断言类型 [__contain_text] ==》实际结果：{}, 期望结果：{} 不相符，断言失败'.format(self.response_data.text, check_data
                                                                           ))
            return self.fail_result

    def run_check(self, check_type, except_result):
        """
        :param check_type: 检查的类型
        :param except_result: 检查的字段
        :return:
        """
        if check_type == "none" or except_result == "":
            return self.function["none"]()
        else:
            return self.function[check_type](except_result)


if __name__ == '__main__':
    import requests

    url = "https://api.weixin.qq.com/cgi-bin/token"
    get_params = {"grant_type": "client_credential", "appid": "wxb637f897f0bf1f0d",
                  "secret": "501123d2d367b109a5cb9a9011d0f084"}

    response = requests.get(url=url, params=get_params)
    # print(response.headers)

    ck = CheckUtils(response)
    # print(ck.none_check())
    # print(ck.run_check('json_key', "access_token,expires_in"))
    # print(ck.run_check('json_key_value', '{"expires_in": 7200}'))
    # print(ck.run_check("body_regexp", '"access_token":"(.+?)"'))
    # print(ck.run_check("header_key_check", "Connection"))
    # print(ck.run_check("header_key_value_check", '{"Connection": "keep-alive"}'))
    # print(ck.run_check("response_code_check", "200"))
    print(ck.run_check("contain_text","access_token"))
