# -*- coding: utf-8 -*-
# @Time : 2021/12/17 15:57
# @Author : Limusen
# @File : logs_utils

from common.config_utils import local_config
from nb_log import LogManager


class LogsUtils:

    def logger(self):
        logger = LogManager('Api_Test').get_logger_and_add_handlers(
            is_add_stream_handler=True,
            log_filename=local_config.LOG_NAME
        )
        return logger


logger = LogsUtils().logger()
