# # -*- coding: utf-8 -*-
# # @Time : 2021/12/22 13:22
# # @Author : Limusen
# # @File : unitest_paramtest_demo_12
#
#
# """
#
# 安装
# pip install paramunittest
#
# """
#
# import unittest
# # import paramunittest
#
# # # 元祖举例
# # @paramunittest.parametrized(
# #     (10, 20, 30), (30, 40, 70), (70, 80, 150)
# # )
# # class ParamUnittestDemo12(paramunittest.ParametrizedTestCase):
# #
# #     def setParameters(self, numa, numb, numc):
# #         self.a = numa
# #         self.b = numb
# #         self.c = numc
# #
# #     def test_add_case(self):
# #         print("%s + %s ?= %s" % (self.a, self.b, self.c))
# #         self.assertEqual(self.a + self.b, self.c)
#
# # # 字典举例
# # @paramunittest.parametrized(
# #     {"numa": 20, "numb": 30, "numc": 50},
# #     {"numa": 40, "numb": 60, "numc": 100},
# #     {"numa": 80, "numb": 110, "numc": 190}
# # )
# # class PUnitTestDemo(paramunittest.ParametrizedTestCase):  # unittest.TestCase
# #
# #     def setParameters(self, numa, numb, numc):
# #         self.a = numa
# #         self.b = numb
# #         self.c = numc
# #
# #     def test_add_case(self):
# #         print("%d + %d ?= %d" % (self.a, self.b, self.c))
# #         self.assertEqual(self.a + self.b, self.c)
#
# cast_info = [
#     {"case_id": "api_case_01", "case_step": ".....1"},
#     {"case_id": "api_case_02", "case_step": ".....2"},
#     {"case_id": "api_case_03", "case_step": ".....3"}
# ]
# # 整合测试用例
# @paramunittest.parametrized(
#     *cast_info
# )
#
#
#
# class PUnitTestDemo(paramunittest.ParametrizedTestCase):  # unittest.TestCase
#
#     def setParameters(self, case_id, case_step):
#         self.case_id = case_id
#         self.case_step = case_step
#
#     def test_add_case(self):
#         print("case_id: %s ,case_step: %s " % (self.case_id, self.case_step))
#         assert True
#
# if __name__ == '__main__':
#     unittest.main(verbosity=2)
