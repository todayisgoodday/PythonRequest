# -*- coding: utf-8 -*-
# @Time : 2021/12/13 13:41
# @Author : Limusen
# @File : assert_demo_09

"""

断言demo

对比json数据

"""

import json

# 单项对比
# key — value 断言基础实现举例
str1 = '{"access_token":"ACCESS_TOKEN","expires_in":7200}'
except_str = '{"expires_in": 7200}'

json_obj = json.loads(str1)
except_obj = json.loads(except_str)
# print(json_obj.items())
# print(except_obj.items())

# 示例2:
a = [(1, 2)]
b = [(2, 3), (1, 2)]
e = []
for c in b:
    if c == a[0]:
        e.append(True)
    else:
        e.append(False)
if False in e:
    print(False)

print(list(except_obj.items())[0])
print(json_obj.items())

# 如果expires_in 在 json_obj列表中则输出ture
if list(except_obj.items())[0] in json_obj.items():
    print(True)
else:
    print(False)


# 多项对比
str1 = '{"access_token":"ACCESS_TOKEN","expires_in":7200}'
except_str_02 = '{"expires_in": 7200,"access_token":"ACCESS_TOKEN"}'
json_obj_02 = json.loads(str1)
except_obj_02 = json.loads(except_str_02)
yes_no = []

for except_res in except_obj_02.items():
    if except_res in json_obj_02.items():
        yes_no.append(True)
    else:
        yes_no.append(False)
if False in yes_no:
    print(False)

