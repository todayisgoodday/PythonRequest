# -*- coding: utf-8 -*-
# @Time : 2022/3/14 11:27
# @Author : Limusen
# @File : 重载

class Name:

    def __init__(self):
        pass

    def name(self, *args):
        return "单纯的一个名字: {}".format(*args)


if __name__ == '__main__':
    name = Name()
    print(name.name("jjjj"))
    print(name.name(""))
