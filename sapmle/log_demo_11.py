# -*- coding: utf-8 -*-
# @Time : 2021/12/17 16:23
# @Author : Limusen
# @File : log_demo_11

from nb_log import LogManager

logger=LogManager('newdream').get_logger_and_add_handlers()
logger.info('你好！')
logger.warning('警告！！')
logger.error('这是错误日志')