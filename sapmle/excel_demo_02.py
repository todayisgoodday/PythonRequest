# -*- coding: utf-8 -*-
# @Time : 2021/12/9 10:23
# @Author : Limusen
# @File : excel_demo_02


import xlrd3

from common.excel_utils import ExcelUtils

excel_data_list = []
data = ExcelUtils("做饭秘籍.xlsx", "Sheet1")

expect_list = [
    {'做菜步骤': '蛋炒饭', '步骤序号': 'step_01', '操作': '油锅烧热', '是否完成': 100.0},
    {'做菜步骤': '蛋炒饭', '步骤序号': 'step_02', '操作': '放油', '是否完成': 100.0},
    {'做菜步骤': '蛋炒饭', '步骤序号': 'step_03', '操作': '放鸡蛋', '是否完成': 100.0},
    {'做菜步骤': '蛋炒饭', '步骤序号': 'step_04', '操作': '放米饭', '是否完成': 80.0},
    {'做菜步骤': '辣椒炒肉', '步骤序号': 'step_01', '操作': '油锅烧热', '是否完成': 100.0},
    {'做菜步骤': '辣椒炒肉', '步骤序号': 'step_02', '操作': '放油', '是否完成': 100.0},
    {'做菜步骤': '辣椒炒肉', '步骤序号': 'step_03', '操作': '放肉', '是否完成': 100.0},
    {'做菜步骤': '辣椒炒肉', '步骤序号': 'step_04', '操作': '放辣椒', '是否完成': 80.0}]


# # 1.获取行的数据 i为循环的次数
# for i in range(1, data.get_row_count()):
#     print(data.get_merged_value(i, 0))

# # 2.获取列的数据
# for b in range(0, data.get_col_count()):
#     print(data.get_merged_value(0,b))

# 3.组装
# print(data.get_row_count())
# print(data.get_col_count())

# 先循环行
for i in range(1, data.get_row_count()):
    # 先定义一个空字典
    row_dict = {}
    # 再根据行数 循环列数
    for j in range(data.get_col_count()):
        # 重新赋值
        row_dict[data.get_merged_value(0, j)] = data.get_merged_value(i, j)
    excel_data_list.append(row_dict)

print(excel_data_list)

