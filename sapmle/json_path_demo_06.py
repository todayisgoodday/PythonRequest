# -*- coding: utf-8 -*-
# @Time : 2021/12/9 15:41
# @Author : Limusen
# @File : json_path_demo_06
import re

import jsonpath

"""

jsonpath 第三方模块

下载方式 
pip install jsonpath

"""
print("========================json_path=========================")
json_data = {"name": "哈利波特", "age": 22, "books": [{"bn": "假如给我三天光明", "p": 32.3}, {"bn": "朝花夕拾", "p": 33.3}]}
# jsonpath返回的是列表 需要用下标取值
# 　$ 表示根节点， .表示字典的key []表示列表的下标
value1 = jsonpath.jsonpath(json_data, "$.age")[0]
print(value1)
value2 = jsonpath.jsonpath(json_data, "$.books[1].bn")[0]
print(value2)
value3 = jsonpath.jsonpath(json_data, "$.books[1].p")[0]
print(value3)
print("========================json_path=========================")

print("========================re=========================")

"""

re模块可以参考: https://www.cnblogs.com/shenjianping/p/11647473.html

"""

# 正则表达式
str_01 = "helloword669582"
str_01 = re.findall("hell(.+?)69582", str_01)[0]
print(str_01)

str1 = "hello123kkkas567"
value4 = re.findall("o(\d+)k", str1)[0]
print(value4)

print("========================re=========================")
