# -*- coding: utf-8 -*-
# @Time : 2021/12/13 13:41
# @Author : Limusen
# @File : assert_demo_09

"""

断言demo

"""

import json


# 断言基础实现举例
str1 = '{"access_token":"12312jkssa"}'
str2 = '{"access_token":"ACCESS_TOKEN","expires_in":7200}'

# 转成json字典
json_obj = json.loads(str1)
json_obj_02 = json.loads(str2)

# 判断字典的键中是否跟 access_token 一样的
# 　对比单个值
if "access_token" in json_obj.keys():
    print(True)
else:
    print(False)

# 对比多个值
check_keys = ["access_token", "expires_in"]
result_list = []

for check_key in check_keys:
    if check_key in json_obj_02.keys():
        print("我是正确的")
        result_list.append(True)
    else:
        print("我是错误的")
        result_list.append(False)
if False in result_list:
    print(False)


print(result_list)

