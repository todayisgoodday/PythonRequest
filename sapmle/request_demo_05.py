# -*- coding: utf-8 -*-
# @Time : 2021/12/9 14:24
# @Author : Limusen
# @File : request_demo_05


import requests

# 先进行简单的使用

import requests

response = requests.get('http://www.baidu.com/')  # 首先调用requests的get方法

print(type(response))  # 获取返回信息的类型
print(response.status_code)  # 获取响应状态码
print(response.text)  # 不需要解码就可以直接打印信息
print(response.cookies)  # 获取网页cookies

# 其他实现方式
requests.post('http://httpbin.org/post')
requests.put('http://httpbin.org/put')
requests.delete('http://httpbin.org/delete')
requests.head('http://httpbin.org/get')
requests.options('http://httpbin.org/get')

# 带参数的get请求
url = 'http://www.baidu.com/s?'
params = {
    "wd" : "测试一下"
}
response = requests.get(url,params=params)
# print(response.content.decode("utf-8"))

# 带参数的post请求
r = requests.post('https://accounts.douban.com/login', data={'form_email': 'abc@example.com', 'form_password': '123456'})
# print(r.content)
