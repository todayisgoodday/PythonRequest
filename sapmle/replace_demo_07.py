# -*- coding: utf-8 -*-
# @Time : 2021/12/9 15:58
# @Author : Limusen
# @File : replace_demo_07


"""

配合re模块进行参数替换

"""

import re

dict_01 = {"token": "88669952sss"}
str1 = '{"access_token":"${token}","name":"${name}"}'

# 找出我们需要替换的值
variables_list = re.findall("\\${.+?}", str1)
print(variables_list)

# 取variables_list列表的第一个值,然后通过 从${ 展位为2   }为-1  分割字段进行替换  \\${.+?}
str1 = str1.replace(variables_list[0], dict_01[variables_list[0][2:-1]])
print(str1)

print(variables_list[0][2:-1])

print(dict_01['token'])