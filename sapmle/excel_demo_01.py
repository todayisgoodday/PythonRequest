# -*- coding: utf-8 -*-
# @Time : 2021/12/8 16:17
# @Author : Limusen
# @File : excel_demo_01

import os
import xlrd3

# 优化获取文件路径的方式
current_path = os.path.dirname(os.path.abspath(__file__))
# 这里分开写是为了适配mac跟windows之间的// 转义,这样更兼容
excel_file_path = os.path.join(current_path, '..', 'testcase_data', '做饭秘籍.xlsx')

# 1.创建工作蒲对象
workbook_object = xlrd3.open_workbook(excel_file_path)

# 2.创建表对象 用的是表Sheet1
sheet_object = workbook_object.sheet_by_name("Sheet1")

# 3.获取指定的行列值
cell_value = sheet_object.cell_value(0, 0)
print(cell_value)

# 4.获取合并单元格的值
cell_value = sheet_object.cell_value(1, 0)
print("获取合并单元格首行的值:", cell_value)  # 获取合并单元格首行的值

cell_value = sheet_object.cell_value(2, 0)
print("获取合并单元格非首行的值: ", cell_value)  # 此时会发现获取不到单元格的值

# 这里会有一个问题 我们有单元格的情况下 获取不到其他格的值

# 5.那我们首先获取这个表格里面有多少个合并单元格
merged_count = sheet_object.merged_cells
# [(1, 5, 0, 1)] [(起始行,结束行/包前不包后,起始列,结束列)]  结束行/结束列都会多1
print(merged_count)  # 会发现在这个表格当中有两个合并单元格

# 5.1举个小例子 来判断这几个数值是否为合并单元格

# x = 2
# y = 0

# # 由此可以看出只有在 x在 1-5之间 y在 0-0之间的才是合并单元格
# if x >= 1 and x < 5:
#     if y >= 0 and y < 1:
#         print("是合并单元格")
#     else:
#         print("不是合并单元格")
# else:
#     print("不是合并单元格-最外层")

# # 就可以将上面的判断改造成for循环
# for (min_row, max_row, min_col, max_col) in sheet_object.merged_cells:
#     print(min_row, max_row, min_col, max_col)


# 6.再次改造
x = 5
y = 0
for (min_row, max_row, min_col, max_col) in sheet_object.merged_cells:

    if x >= min_row and x < max_row:
        if y >= min_col and y < max_col:
            # 是合并单元格,单元格的值等于 合并单元格中的第一个单元格的值
            cell_value = sheet_object.cell_value(min_row, min_col)
            break
        else:
            cell_value = sheet_object.cell_value(x, y)
    else:
        cell_value = sheet_object.cell_value(x, y)


# 7.封装成一个小方法

def get_merged_value(row_index, col_index):
    for (min_row, max_row, min_col, max_col) in sheet_object.merged_cells:

        if row_index >= min_row and row_index < max_row:
            if col_index >= min_col and col_index < max_col:
                # 是合并单元格,单元格的值等于 合并单元格中的第一个单元格的值
                cell_value = sheet_object.cell_value(min_row, min_col)
                break
            else:
                cell_value = sheet_object.cell_value(row_index, col_index)
        else:
            cell_value = sheet_object.cell_value(row_index, col_index)
    return cell_value


print(get_merged_value(4, 0))

# 测试一下
for i in range(0, 9):
    for j in range(0, 4):
        cell_value = get_merged_value(i, j)
        print(cell_value, end="  ")
    print()
