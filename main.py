# -*- coding: utf-8 -*-
# @Time : 2021/12/22 14:13
# @Author : Limusen
# @File : main

"""

测试用例的运行入口

"""

import os
import unittest
from common.config_utils import local_config
from common import HTMLTestReportCN

current = os.path.dirname(os.path.abspath(__file__))
case_path = os.path.join(current, local_config.CASE_PATH)  # 在ini文件写入测试用例地址
report_path = os.path.join(current, local_config.REPORT_PATH)  # 在ini文件写入测试报告地址


def load_api_case():
    discover_obj = unittest.defaultTestLoader.discover(start_dir=case_path,
                                                       pattern='test_api_case.py')
    all_case_suite = unittest.TestSuite()
    all_case_suite.addTest(discover_obj)
    return discover_obj


if __name__ == '__main__':
    # 创建网页版报告路径的对象
    report_path_obj = HTMLTestReportCN.ReportDirectory(report_path)
    # 创建报告
    report_path_obj.create_dir("接口测试报告")
    # 获取网页版报告的路径
    html_report_obj = HTMLTestReportCN.GlobalMsg.get_value("report_path")
    # print(html_report_obj)
    html_report_file_obj = open(html_report_obj, 'wb')
    runner = HTMLTestReportCN.HTMLTestRunner(stream=html_report_file_obj,
                                             tester="me",
                                             description="接口测试框架报告")
    runner.run(load_api_case())
